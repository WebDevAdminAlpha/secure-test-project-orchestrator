# frozen_string_literal: true

# Module for reporting failed pipelines for the secure test pipelines
module SlackSuccessReport
  require_relative 'http_utils'

  SUCCESS_MESSAGE = '🎉 QA against Secure test projects passed! 🎉 See '
  CHANNEL = 's_secure-ops'

  def self.report_headline_to_slack
    HttpUtils.report_to_slack("#{SUCCESS_MESSAGE}#{ENV['CI_PIPELINE_URL']}",
                              ':ci_passing:',
                              CHANNEL)
  end
end

SlackSuccessReport.report_headline_to_slack
